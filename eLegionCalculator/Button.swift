//
//  Button.swift
//  eLegionCalculator
//
//  Created by IvanLazarev on 13/05/2018.
//  Copyright © 2018 IvanLazarev. All rights reserved.
//

import Foundation
import UIKit


class Button: UIButton {
    override open var isSelected: Bool {
        didSet {
            alpha = isSelected ? 0.5 : 1
        }
    }
}
