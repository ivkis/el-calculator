//
//  ViewController.swift
//  eLegionCalculator
//
//  Created by IvanLazarev on 01/05/2018.
//  Copyright © 2018 IvanLazarev. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    let calculator = CalculatorLogic()
    var prevSelectedButton: UIButton?

    @IBOutlet weak var displayResultLabel: UILabel!
    @IBOutlet var landscapeUIConstraint: NSLayoutConstraint!
    @IBOutlet weak var mrButton: UIButton!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func viewDidLoad() {
        updateUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleOrientation(UIDevice.current.orientation)
    }

    func handleButtonSelection(button: UIButton, select: Bool) {
        prevSelectedButton?.isSelected = false
        button.isSelected = select
        prevSelectedButton = button
    }

    func handleOrientation(_ orientation: UIDeviceOrientation) {
        if orientation.isLandscape {
            landscapeUIConstraint.isActive = true
        } else {
            landscapeUIConstraint.isActive = false
        }
    }

    func updateUI() {
        displayResultLabel.text = calculator.displayValue
        mrButton.isSelected = calculator.memoryValue != 0
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        handleOrientation(UIDevice.current.orientation)
    }

    @IBAction func numberPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        let number = sender.currentTitle!
        calculator.inputNumber(number)
        updateUI()
    }

    @IBAction func twoOperandsSignPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: true)
        calculator.doBinary(operation: CalculatorLogic.BinaryOperation(rawValue: sender.currentTitle!)!)
        updateUI()
    }

    @IBAction func oneOperandSignPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        calculator.doUnary(operation: CalculatorLogic.UnaryOperation(rawValue: sender.currentTitle!)!)
        updateUI()
    }

    @IBAction func equalitySignPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        calculator.doEquals()
        updateUI()
    }

    @IBAction func clearButtonPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        calculator.clear()
        updateUI()
    }

    @IBAction func dotButtonPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        calculator.doDot()
        updateUI()
    }

    @IBAction func memoryButtonPressed(_ sender: UIButton) {
        handleButtonSelection(button: sender, select: false)
        calculator.doMemory(operation: CalculatorLogic.MemoryOperation(rawValue: sender.currentTitle!)!)
        updateUI()
    }
}
