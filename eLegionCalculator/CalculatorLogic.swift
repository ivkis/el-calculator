//
//  CalculatorLogic.swift
//  eLegionCalculator
//
//  Created by IvanLazarev on 04/05/2018.
//  Copyright © 2018 IvanLazarev. All rights reserved.
//

import Foundation


class CalculatorLogic {

    enum UnaryOperation: String {
        case plusMinus = "+/-"
        case percentage = "%"
        case root = "√"
        case sin = "sin"
        case cos = "cos"
        case tan = "tan"
        case eulerNumber = "e"
        case xSquared = "x²"
        case xCube = "x³"
        case pi = "π"
    }

    enum BinaryOperation: String {
        case plus = "+"
        case minus = "-"
        case multiplication = "×"
        case division = "÷"
        case xPowerOfa = "xª"
    }

    enum MemoryOperation: String {
        case clearMemory = "mc"
        case addMemory = "m+"
        case subtractMemory = "m-"
        case takeMemory = "mr"
    }

    enum DefaultsKeys {
        static let displayValue = "displayValue"
        static let memoryValue = "memoryValue"
    }
    let defaults = UserDefaults.standard
    fileprivate var stillTyping = false
    fileprivate var dotIsPlaced = false
    fileprivate var firstOperand: Double = 0
    fileprivate var secondOperand: Double = 0
    var operationSign: BinaryOperation?

    var displayValue: String {
        didSet {
            defaults.set(displayValue, forKey: DefaultsKeys.displayValue)
        }
    }

    var memoryValue: Double {
        didSet {
            defaults.set(memoryValue, forKey: DefaultsKeys.memoryValue)
        }
    }

    fileprivate var currentInput: Double {
        get {
            return Double(displayValue)!
        }
        set {
            let value = "\(newValue)"
            let valueArray = value.components(separatedBy: ".")
            if valueArray[1] == "0" {
                displayValue = "\(valueArray[0])"
            } else {
                displayValue = "\(newValue)"
            }
            stillTyping = false
        }
    }

    init() {
        self.displayValue = defaults.string(forKey: DefaultsKeys.displayValue) ?? "0"
        self.memoryValue = defaults.double(forKey: DefaultsKeys.memoryValue)
    }

    func inputNumber(_ number: String) {
        if stillTyping {
            if displayValue.count < 20 {
                displayValue = displayValue + number
            }
        } else {
            displayValue = number
            stillTyping = true
        }
    }

    func operateWithOneOperand(operation: (Double) -> Double) {
        currentInput = operation(currentInput)
        stillTyping = false
    }

    func operateWithTwoOperands(operation: (Double, Double) -> Double) {
        currentInput = operation(firstOperand, secondOperand)
        stillTyping = false
    }

    func clear() {
        firstOperand = 0
        secondOperand = 0
        currentInput = 0
        displayValue = "0"
        stillTyping = false
        dotIsPlaced = false
        operationSign = nil
    }

    func doUnary(operation: UnaryOperation) {
        switch operation {
        case .plusMinus:
            operateWithOneOperand{-$0}
        case .percentage:
            operateWithOneOperand{firstOperand * $0 / 100}
        case .root:
            operateWithOneOperand{sqrt($0)}
        case .sin:
            operateWithOneOperand{sin($0 * Double.pi / 180)}
        case .cos:
            operateWithOneOperand{cos($0 * Double.pi / 180)}
        case .tan:
            operateWithOneOperand{tan($0 * Double.pi / 180)}
        case .eulerNumber:
            operateWithOneOperand{_ in exp(1)}
        case .xSquared:
            operateWithOneOperand{$0 * $0}
        case .xCube:
            operateWithOneOperand{$0 * $0 * $0}
        case .pi:
            operateWithOneOperand{_ in Double.pi}
        }
    }

    func doBinary(operation: BinaryOperation) {
        operationSign = operation
        firstOperand = currentInput
        stillTyping = false
        dotIsPlaced = false
    }

    func doMemory(operation: MemoryOperation) {
        switch operation {
        case .clearMemory:
            memoryValue = 0
        case .addMemory:
            memoryValue = memoryValue + currentInput
        case .subtractMemory:
            memoryValue = memoryValue - currentInput
        case .takeMemory:
            currentInput = memoryValue
        }
        stillTyping = false
    }

    func doEquals() {
        secondOperand = currentInput
        dotIsPlaced = false
        guard let operation = operationSign else {
            return
        }
        switch operation {
        case .plus:
            operateWithTwoOperands{$0 + $1}
        case .minus:
            operateWithTwoOperands{$0 - $1}
        case .multiplication:
            operateWithTwoOperands{$0 * $1}
        case .division:
            operateWithTwoOperands{$0 / $1}
        case .xPowerOfa:
            operateWithTwoOperands{pow($0,$1)}
        }
    }

    func doDot() {
        if stillTyping && !dotIsPlaced {
            displayValue = displayValue + "."
            dotIsPlaced = true
        } else if !stillTyping && !dotIsPlaced {
            displayValue = "0."
        }
    }
}
