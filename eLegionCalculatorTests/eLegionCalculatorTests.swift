//
//  eLegionCalculatorTests.swift
//  eLegionCalculatorTests
//
//  Created by IvanLazarev on 09/05/2018.
//  Copyright © 2018 IvanLazarev. All rights reserved.
//

import XCTest
@testable import eLegionCalculator


class eLegionCalculatorTests: XCTestCase {
    var calculator: CalculatorLogic!
    
    override func setUp() {
        super.setUp()
        calculator = CalculatorLogic()
    }
    
    func testCanEnterNumber() {
        calculator.inputNumber("1")
        calculator.inputNumber("2")
        XCTAssertEqual(calculator.displayValue, "12")
    }

    func testPlus() {
        calculator.inputNumber("5")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("7")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "12")
    }

    func testMinus() {
        calculator.inputNumber("12")
        calculator.doBinary(operation: .minus)
        calculator.inputNumber("7")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "5")
    }

    func testMultiplication() {
        calculator.inputNumber("11")
        calculator.doBinary(operation: .multiplication)
        calculator.inputNumber("43")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "473")
    }

    func testDivision() {
        calculator.inputNumber("17")
        calculator.doBinary(operation: .division)
        calculator.inputNumber("2")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "8.5")
    }

    func testPowerOf() {
        calculator.inputNumber("2")
        calculator.doBinary(operation: .xPowerOfa)
        calculator.inputNumber("6")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "64")
    }

    func testPlusMinus() {
        calculator.inputNumber("58")
        calculator.doUnary(operation: .plusMinus)
        XCTAssertEqual(calculator.displayValue, "-58")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("78")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "20")
        calculator.doUnary(operation: .plusMinus)
        XCTAssertEqual(calculator.displayValue, "-20")
    }

    func testPercentage() {
        calculator.inputNumber("100")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("40")
        calculator.doUnary(operation: .percentage)
        XCTAssertEqual(calculator.displayValue, "40")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "140")
        calculator.doBinary(operation: .minus)
        calculator.inputNumber("70")
        calculator.doUnary(operation: .percentage)
        XCTAssertEqual(calculator.displayValue, "98")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "42")
    }

    func testRoot() {
        calculator.inputNumber("16")
        calculator.doUnary(operation: .root)
        XCTAssertEqual(calculator.displayValue, "4")
    }

    func testEquals() {
        calculator.inputNumber("2")
        calculator.doBinary(operation: .plus)
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "4")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "6")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "8")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "10")
    }

    func testSin() {
        calculator.inputNumber("45")
        calculator.doUnary(operation: .sin)
        XCTAssertEqual(calculator.displayValue, "0.707106781186547")
        calculator.clear()
        XCTAssertEqual(calculator.displayValue, "0")
        calculator.inputNumber("2")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("90")
        calculator.doUnary(operation: .sin)
        XCTAssertEqual(calculator.displayValue, "1")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "3")
    }

    func testCos() {
        calculator.inputNumber("60")
        calculator.doUnary(operation: .cos)
        XCTAssertEqual(calculator.displayValue, "0.5")
        calculator.clear()
        XCTAssertEqual(calculator.displayValue, "0")
        calculator.inputNumber("7")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("60")
        calculator.doUnary(operation: .cos)
        XCTAssertEqual(calculator.displayValue, "0.5")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "7.5")
    }

    func testTan() {
        calculator.inputNumber("45")
        calculator.doUnary(operation: .tan)
        XCTAssertEqual(calculator.displayValue, "1")
        calculator.inputNumber("7")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("135")
        calculator.doUnary(operation: .tan)
        XCTAssertEqual(calculator.displayValue, "-1")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "6")
    }

    func testConstantNumbers() {
        calculator.doUnary(operation: .pi)
        XCTAssertEqual(calculator.displayValue, "3.14159265358979")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("4")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "7.14159265358979")
        calculator.doBinary(operation: .minus)
        calculator.doUnary(operation: .eulerNumber)
        XCTAssertEqual(calculator.displayValue, "2.71828182845905")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "4.42331082513074")

    }

    func testXSquared() {
        calculator.inputNumber("2")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("5")
        calculator.doUnary(operation: .xSquared)
        XCTAssertEqual(calculator.displayValue, "25")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "27")
    }

    func testXCube() {
        calculator.inputNumber("27")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("3")
        calculator.doUnary(operation: .xCube)
        XCTAssertEqual(calculator.displayValue, "27")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "54")
    }

    func testMemoryAdd() {
        calculator.inputNumber("5")
        calculator.doMemory(operation: .addMemory)
        XCTAssertEqual(calculator.memoryValue, 5)
        calculator.inputNumber("12")
        XCTAssertEqual(calculator.displayValue, "12")
        calculator.doBinary(operation: .plus)
        calculator.inputNumber("11")
        calculator.doEquals()
        XCTAssertEqual(calculator.displayValue, "23")
        calculator.doMemory(operation: .addMemory)
        XCTAssertEqual(calculator.memoryValue, 28)
        calculator.doMemory(operation: .takeMemory)
        XCTAssertEqual(calculator.displayValue, "28")
        calculator.doMemory(operation: .addMemory)
        XCTAssertEqual(calculator.memoryValue, 56)
        calculator.doMemory(operation: .addMemory)
        XCTAssertEqual(calculator.memoryValue, 84)
        calculator.doMemory(operation: .addMemory)
        XCTAssertEqual(calculator.memoryValue, 112)
        calculator.doMemory(operation: .clearMemory)
        XCTAssertEqual(calculator.memoryValue, 0)
    }

    func testMemorySubtract() {
        calculator.inputNumber("34")
        calculator.doMemory(operation: .addMemory)
        calculator.inputNumber("14")
        calculator.doMemory(operation: .subtractMemory)
        XCTAssertEqual(calculator.memoryValue, 20)
        calculator.doMemory(operation: .clearMemory)
    }

    func testMemoryTake() {
        calculator.inputNumber("23")
        calculator.doMemory(operation: .addMemory)
        calculator.clear()
        XCTAssertEqual(calculator.displayValue, "0")
        calculator.doMemory(operation: .takeMemory)
        XCTAssertEqual(calculator.displayValue, "23")
        calculator.doMemory(operation: .clearMemory)
    }
}
